public class BikeStore {
    public static void main(String[] args)
    {
        Bicycle[] bicycles = new Bicycle[4];
        bicycles[0] = new Bicycle("Garneau", 10, 40);
        bicycles[1] = new Bicycle("DCO", 7, 35);
        bicycles[2] = new Bicycle("Specialized", 10, 50);
        bicycles[3] = new Bicycle("Specialized", 5, 25);

        for (int i = 0; i < bicycles.length; i++)
        {
            System.out.println(bicycles[i]);
        }
    }
}
